// let movies = ['One Piece' , 'Jujutsu Kaisen', 'Naruto', 'Blue Period' , 
// 'Attack on Titan'];

// console.log (movies[0]); //OP
// console.log (movies[1]); //JK
// console.log (movies[2]); // NARUTO

// // change values 

// movies [1] = 'Shonen Anime'; 
// console.log(movies[1]);
// console.log (movies.length);
// console.log (movies.length-1);


// //initialize empty array 

// // let colors =[0];


// //array can hold any types of data 

// // let random_collection = [49,true, 'Satoru',null];

// //array have a length property 

// //let nums = [45,37,89,24];
// // console.log (nums.length)




// // ===== ♥ARRAY METHODS♥ ===== // 

// // property - parang noun? 
// // method - verb, what an array can do or perform , WITH (), function is nakapaloob sa isang bagay or array 

// // ♥ PUSH METHOD

// // to add property 
// // let colors = ['red', 'orange','yellow'];
// //console.log (colors) 
// //colors.push ('green');
// // ['red', 'orange', 'yellow', 'green']

// // ♥ POP METHOD 


// //♥ UNSHIFT - to add to the front of an array 
// // colors.unshift('infrared');
// // console.log (colors);
// //output :  ['infrared', 'red', 'orange', 'yellow']

// //♥ SHIFT - remove the first item in an array 

// // console.log(colors.shift());
// // console.log(colors);
// //output : ['orange', 'yellow']

// //♥INDEXOF - to find an the index of an item in an array 

// let tuitt = ['Charles' , 'Paul', 'Sef', 'Alex', 'Paul'];
// console.log(tuitt.indexOf('Sef')); //2
// console.log(tuitt.indexOf('Paul')) // 1 - first instance 
// console.log(tuitt.indexOf('Hulk')) // - 1  - not found 

// //// ====== ARRAY ITERATIONS ======= //// 

// //LOOPS : initial value | argument | increment 

// // • FOR LOOP - we need to know the length of the array 

// let colors = ['red','orange','yellow','green'];

// for(let i = 0; i < colors.length; i++) 
// {
//     colors.forEach(function (color) {
//         console.log (`${colors[i]} is in ${i}` )
//     });
//     console.log(colors [i]); // index , red and yellow output 
// }

// // • FOR EACH - is a built-in method for iterating over an array 
// //- needs function to work 


// // colors.forEach(function(color) { // () color in here is just a placeholder 
// //     console.log(color); // output : red orange yellow green
// // });

// //• sort
// //• reverse 
// //• 
// //•

/////  ••• REFERENCE TYPES •••• ///

// every time you make an array, kahit same content, it is "housed"
// in its own a different loc
let colors = ['red', 'green', 'blue']
let colors2 = ['red', 'green', 'blue'] // colors and colors2 are false 
let colors3 = colors; // colors is being referenced?  both will be updated -- true

// const colors = ['red', 'green', 'blue'] // this can still change (the content) 
//but it cannot change where it is stored

const scores = [
    [12,15,16],
    [11,9,8],
    [1,5,6],
]

// console.log (scores [1][2]) // output = 8  INDEXX!!!

scores.forEach(function(score){
    console.log(score)
    });

for(let i = 0; i < scores.length; i++){
    console.log (scores[i])
    //another loop for scpres [i]
    for (let j = 0; j < scores[i].length; j++){
        console.log(scores[i][j])
    }
};


//////=====OBJECTS=====/////////

const grades = [98,87,91,84]

const myGrades = {
    lastName: 'Gojo',
    firstName: 'Satoru',
    hobbies: ['drinking coffee', 'cosplay', 'gaming'],
    science: 98,
    math: 87,
    english: 91,
    programming: 84,
}
//key - science value - 98
// to access objects use dot notation- myGrades.science = 98 

myGrades.hobbies[1] // to access cosplay 


const person = {
    firstName:'Cloud',
    lastName: 'Strife',
    email: ['firstclass@yahoo.com', 'geostigma@gmail.com' ],
    address: {
        city: 'Midgar', 
        country: 'Japan',
        //person.address.country - to access japan 
    }, 
    greeting: function(){ //-methods inside an objects
        return 'ohayo!';
    }
}

const shoppingCart= [
    {
        product: 'shirt',
        price: 99.90,
        qty: 2,
        discount: 10
    },
    {
        product: 'watch',
        price: 9.90,
        qty: 1,
    },
    {
        product: 'laptop',
        price: 999.90,
        qty: 2,
    },
]

const blogger = {
    lname: 'Smith',
    fname: 'John',
    posts :[
        {
            title: 'My first post',
            body: 'Lorem Ipsum',
            comments: [
                {}
            ]
        },
        {

        },{

        }
    ]
}


