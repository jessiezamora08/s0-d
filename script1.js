function sum(num1, num2) {
    return num1 + num2;
}

///// ===== ARROW FUNCTIONS ===== \\\\\

const sum2 = (num1, num2) => num1 + num2 // 1st 

const isPositive = num => num >= 0; // 2nd 

// function randNum() {
//     return Math.random()
// }

const randNum = () => Math.random()

let recipe = {
    title: "Tonkatsu",
    servings: 2,
    ingredients: ['panko', 'flour', 'egg', 'chicken breast', 'salt', 'pepper'],
    feedback: () => console.log('Delicious!')
}

/// ARRAY OF OBJECTS 
let mangas = [
    {
        title: ['One Piece'],
        author: ['Eiichiro Oda'],
        alreadyRead: true
    },
    {
        title: ['Naruto'], // to access this manga[1].title
        author: ['Masashi Kishimoto'],
        alreadyRead: false
    },
    {
        title: ['Reborn'],
        author: ['Akira Amano'],
        alreadyRead: true
    }
]
mangas.forEach((manga) => {
    console.log(manga.title)     // all titles will display forEach
});

for (let i = 0; i < mangas.length; i++) {
    console.log(mangas[i].title)  // all titles will display forLoop
}

for (let i = 0; i < mangas.length; i++) {
    if (mangas[i].alreadyRead === false)
        console.log(mangas[i].title) // title with false
}

mangas.forEach((mangas) => {
    if (mangas.alreadyRead === false)
        console.log(mangas.title);
});

const person = {
    firstName: 'Cloud',
    lastName: 'Strife',
    email: ['firstclass@yahoo.com', 'geostigma@gmail.com'],
    address: {
        city: 'Midgar',
        country: 'Japan',
        //person.address.country - to access japan 
    },
    // greeting:()=> { //-methods inside an objects
    //     return this.firstName + ' ' + this.lastName; ..output: undefined
    // }

    greeting1: function () {
        return this.firstName + ' ' + this.lastName;
    },  // outout : Cloud Strife , this = refers to itself, needed

    greeting: function () {
        return this.address.city + ' ' + this.address.country;
    }  //  Midgar Japan
}


let mangas2 = [
    {
        title: ['One Piece'],
        author: ['Eiichiro Oda'],
        alreadyRead: true,
        rating: 5
    },
    {
        title: ['Naruto'], // to access this manga[1].title
        author: ['Masashi Kishimoto'],
        alreadyRead: false,
        rating: 4
    },
    {
        title: ['Reborn'],
        author: ['Akira Amano'],
        alreadyRead: true,
        rating: 4
    }
    
]

for (let i = 0; i < mangas2.length; i++) {
    if (mangas2[i].rating >= 3)
        console.log(mangas2[i].title)
        console.log(mangas2[i].rating) // title with false
}

mangas2.forEach((mangas2) => {
    if (mangas2.rating >= 3)
        console.log(mangas2.title);
        console.log(mangas2.rating);
});


const myGrades = {
    science: 98,
    math: 87,
    english: 81,
    programming: 99,

    topScore: () => (Math.max(98, 87, 81, 99))
}

// const randomNum = (num) => {
//     if (num === undefined){
//         num =6 
//     }
//     return Math.floor(Math.random()*num) + 1;
// }

//Default Params 

const randomNum = (num=6) => {
    return Math.floor(Math.random()*num) + 1;
}


////// ====== REST API ===== \\\\\\\

/* API - has endpoint  

JSON - JS Object Notation 
- standard text based format for representing 
- uses key value pairs
